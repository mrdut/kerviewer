#!/usr/bin/python

import os, sys, pwd
import Image
import math
from PyQt4 import QtGui, QtCore

__ROOT_DIR__ = "/home/mrdut/Pictures2/"
__THUMBNAIL_DIR__ = __ROOT_DIR__+".thumbnail/"

__THUMBNAIL_HEIGHT__ = 200


class SelectionManager(QtGui.QStandardItemModel):

    image_added_signal = QtCore.pyqtSignal(str,str,name='imageAdded')
    image_removed_signal = QtCore.pyqtSignal(str,str,name='imageRemoved')
    selection_loaded_signal = QtCore.pyqtSignal(name='selectionLoaded')

    def __init__(self):
        QtGui.QStandardItemModel.__init__(self)
        self.setHorizontalHeaderLabels(['Name'])


        self.filename = QtCore.QString()

        self.changed = False



    def hasImage(self,path,name):
        """
        return true if image is present in selection, false otherwise
        """
        return self.imageItem(path,name) is not None

    def hasPath(self,path):
        """
        return true if path is present in selection, false otherwise
        """
        return self.pathItem(path,name) is not None

    def toggleImage(self,path,name):
        """
        if image item is present, remove it
        if image item is not present, add it
        """
        if self.hasImage(path,name)==False:
            self.addImage(path,name)
        else:
            self.removeImage(path,name)

    def removeImage(self,path,name):
        
        # remove image item
        pathItem = self.pathItem(path)
        i = self.imageRow(pathItem,name)
        pathItem.removeRow(i)

        # remove path item if no more image item left
        if pathItem.rowCount() == 0:
            index = self.indexFromItem(pathItem)
            self.removeRow( index.row() )

        self.changed = True
        self.image_removed_signal.emit(path,name)


    def addImage(self,path,name):
        pathItem = self.pathItem(path) or self.createPathItem(path)
        image = self.imageItem(pathItem,name) or self.createImageItem(pathItem,name)
        self.changed = True
        self.image_added_signal.emit(path,name)

    def imageIndex(self,path,name):
        """
        return image index
        """
        pathItem = self.pathItem(path)
        return self.indexFromItem( self.imageItem(pathItem,name) ) if pathItem is not None else None

    def pathIndex(self,path):
        """
            path: str
        """
        item = self.pathItem(path)
        return self.indexFromItem(item) if (item is not None) else None

    def pathItem(self,path):
        """
        Look for path item

        Parameters
        ----------
        path: str
            The path of the item we are looking for

        Returns
        -------
        item: QModelIndex
            The model index of the item, or None if it does not exists
        """
        items = self.findItems(path)
        return items[0] if len(items) != 0 else None

    def imageRow(self,pathItem,name):


        if isinstance(pathItem, QtCore.QString):

            return self.imageRow( self.pathItem(path) ,name)

        else:

            assert( isinstance(pathItem,QtGui.QStandardItem) )

            for i in range(pathItem.rowCount()):
                if pathItem.child(i).text() == name:
                    return i

            assert False # should never reach this point

    def imageItem(self,path,name):
        """
        returns image item given its name and path, or None if it is not in selection
        """

        if isinstance(path, QtCore.QString):

            return self.imageItem( self.pathItem(path) ,name)

        else:

            if path is not None:
                assert( isinstance(path,QtGui.QStandardItem) )

                for i in range(path.rowCount()):
                    if path.child(i).text() == name:
                        return path.child(i)
            return None

    def createImageItem(self,pathItem,name):
        """
        create image item from pathItem and returns it
        """
        item = QtGui.QStandardItem(name)
        item.setIcon(  QtGui.QIcon.fromTheme("image-x-generic") )
        pathItem.appendRow( [item] )
        return item

    def createPathItem(self,path):
        """
        create path item and returns it
        """
        item = QtGui.QStandardItem(path)
        item.setIcon(  QtGui.QIcon.fromTheme("folder") )
        self.appendRow([item])
        return item

    def load(self,filename):

        assert filename.endsWith(".sel")

        self.filename = filename

        with open(str(self.filename),"r") as f:

            # remove all items from current selection
            self.clear()

            # read all lines
            lines = f.readlines()

            # add items
            for line in lines:
                dirname,basename = os.path.split(line.rstrip())
                self.addImage(dirname,basename)

            self.changed = False

            self.selection_loaded_signal.emit()


        print "Selection",self.filename,"loaded"

        
    def imageStringList(self):

        arr = []
        for row in range(self.rowCount()):
            root = self.item( row )
            for i in range(root.rowCount()):
                arr.append( str(root.text()+"/"+root.child(i).text()) )

        return arr

    def export(self):
        """
        save current selection to mozaic
        """
        
        assert not self.filename.isEmpty()

        images = self.imageStringList()

        thumbnails = self.createThumbnails(images)

        mosaicname = self.filename.replace(".sel",".html")

        basename = os.path.basename(str(self.filename))

        src = ['<img src="'+thumb+'">' for thumb in thumbnails]

        with open(mosaicname,"w") as f:


            f.write('<!DOCTYPE html> \
            <html>\
                <head>\
                    <meta charset="utf-8">\
                    <title>'+basename+'</title>\
                    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">\
                    <style>\
                        body { background-color:lightgrey; }\
                        h1   { text-align: center; font: normal 20px Roboto,arial,sans-serif; }\
                        #mosaic { width:1360px; margin: auto auto; }\
                        img { border:2px solid white; float: left; }\
                    </style>\
                </head>\
                <body>\
                    <h1>'+basename+'</h1>\
                    <div id="mosaic">'+
                        '\n'.join(src) + 
                    '</div>\
                </body>\
            </html>')

    def createThumbnails(self,_images=None):
        """
        create thumbnail image for each image in selection
        """

        if not os.path.isdir(__THUMBNAIL_DIR__):
            print "create directory :",__THUMBNAIL_DIR__
            os.mkdir( __THUMBNAIL_DIR__ )

        images = _images or self.imageStringList()
        thumbnails = [ image.replace(__ROOT_DIR__,__THUMBNAIL_DIR__) for image in images ]

        for img,thumb in zip(images,thumbnails):
            
            dirname = os.path.dirname(thumb)
            
            if not os.path.exists(dirname):
                print "create directory :",dirname
                os.makedirs(dirname)
            
            if not os.path.isfile(thumb):

                try:
                    im = Image.open(img)


                    size = math.floor(float(im.size[0])/float(im.size[1])*float(__THUMBNAIL_HEIGHT__)),__THUMBNAIL_HEIGHT__
                    print im.size,size
                    im.thumbnail(size)
                    im.save(thumb, "JPEG")
                except IOError:
                    print "cannot create thumbnail for", thumb
        print "done thumbnails"

        return thumbnails

    def save(self,filename):

        if not filename.endsWith(".sel"):
            filename += ".sel"

        self.filename = filename

        txt = '\n'.join(self.imageStringList())

        with open(str(self.filename),"w") as f:
            f.write(txt)

            self.changed = False

        print "Saved selection as",self.filename

    def keyPressEvent(self, event):
        print "[SelectionManager] key pressed",event
        # if type(event) == QtGui.QKeyEvent:
            # key = event.key()
            # print "[SelectionManager] key pressed",event.text()

class SelectionView(QtGui.QTreeView):

    current_image_changed_signal = QtCore.pyqtSignal(str,str,name='currentImageChanged')

    def __init__(self,model):
        QtGui.QTreeView.__init__(self)

        self.setModel(model)
        
        self.selectionModel().currentChanged.connect( self.printImage)

    def printImage(self,index):
        item = self.model().itemFromIndex(index)


        # ensure it is image item 
        if item is not None and item.parent() is not None:

            name = item.text()
            path = item.parent().text()

            self.current_image_changed_signal.emit(path,name)


    def setCurrentImage(self,path,name):
        index = self.model().imageIndex(path,name)
        if index is not None:
            self.setCurrentIndex(index)


    def load(self):
        
        if self.changed():
            self.saveAs()

        default = self.model().filename if not self.model().filename.isEmpty() else __ROOT_DIR__

        filename = QtGui.QFileDialog.getOpenFileName(self,"Load Selection", default , "Selection File (*.sel)")

        if not filename.isEmpty():
            self.model().load(filename)



    def saveAs(self):

        default = self.model().filename if not self.model().filename.isEmpty() else __ROOT_DIR__

        filename = QtGui.QFileDialog.getSaveFileName(self,"Save Selection", default , "Selection File (*.sel)")

        if not filename.isEmpty():
            self.model().save(filename)

    def export(self):

        if not self.model().filename.isEmpty():
            self.model().export()

        else:
            print "please save selection before"

    def changed(self):
        return self.model().changed

class MyQDirModel(QtGui.QDirModel):

    def __init__(self):
        QtGui.QDirModel.__init__(self)
        self.selection_model = SelectionManager()

    def data(self, index, role = QtCore.Qt.DisplayRole):

        if index.isValid() and self.isIndexCheckable(index) and (role == QtCore.Qt.CheckStateRole):
            return [QtCore.Qt.Unchecked,QtCore.Qt.Checked][self.selection_model.hasImage( *self.imageInfo(index) )]

        return QtGui.QDirModel.data(self, index, role)        

    def isIndexCheckable(self,index):
        return not self.isDir(index) and (index.column()==0)

    def flags(self, index):

        if self.isIndexCheckable(index):
           return QtGui.QDirModel.flags(self, index) | QtCore.Qt.ItemIsUserCheckable
        else:
            return QtGui.QDirModel.flags(self, index)            

    def imageInfo(self,index):
        info = self.fileInfo(index)
        path = info.absolutePath()
        name = info.fileName()
        return path,name

    def toggleCheckState(self, index):

        lut = { 
                QtCore.Qt.Unchecked : QtCore.Qt.Checked,
                QtCore.Qt.Checked : QtCore.Qt.Unchecked
            }

        old_value = self.data(index,QtCore.Qt.CheckStateRole)
        new_value = lut[old_value]
        
        self.setData(index,new_value,QtCore.Qt.CheckStateRole)

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        if index.isValid() and self.isIndexCheckable(index) and role == QtCore.Qt.CheckStateRole:

            if (value == QtCore.Qt.Checked):
                self.selection_model.addImage( *self.imageInfo(index) )
                self.dataChanged.emit(index,index)
                return True
            else:
                self.selection_model.removeImage( *self.imageInfo(index) )
                self.dataChanged.emit(index,index)
                return True

        else:
            return QtGui.QDirModel.setData(self, index, value, role)

class ImageDirectoryManager(QtGui.QTreeView):

    """
    blabla
    """

    image_selected_signal = QtCore.pyqtSignal(str,str,name='imageSelected')

    # signal emitted when current image changed (either by selection or by click)
    current_image_changed_signal = QtCore.pyqtSignal(str,str,name='currentImageChanged')

    def __init__(self):
        QtGui.QTreeView.__init__(self)

        self.setModel( MyQDirModel() )


        # username = pwd.getpwuid( os.getuid() )[ 0 ]
        # self.rootDir = "/home/" + username + "/"
        # if os.path.isdir(self.rootDir):
        #     if os.path.isdir(self.rootDir+"Pictures2"):
        #         self.rootDir += "Pictures2"
        #     elif os.path.isdir(self.rootDir+"Images"):
        #         self.rootDir += "Images"
        self.setRootIndex(self.model().index(__ROOT_DIR__))


        self.model().setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot | QtCore.QDir.AllEntries)
        self.model().setNameFilters(["*.jpeg","*.jpg","*.png"])

        self.header().resizeSection(0, 250)
        self.header().resizeSection(1, 70)
        self.header().resizeSection(2, 75)

        self.clicked.connect(lambda index : self.proxy( self.current_image_changed_signal, index) )
        self.selectionModel().currentChanged.connect(lambda index : self.proxy( self.current_image_changed_signal, index) )

        self.model().dataChanged.connect( self.repaint )
        self.model().dataChanged.connect( lambda index,index2 : self.proxy( self.current_image_changed_signal, index) )

    def proxy(self,signal,index):

        # TODO : def isImage(self,index)
        if not self.model().isDir(index):
            # print "[ImageDirectoryManager::proxy] path='%s', name='%s'"%(path,name)
            signal.emit( *self.model().imageInfo(index) )

    def setCurrentImage(self,path,name):


        dirname,basename = self.currentImage()

        # ensure item is not already selected
        if dirname!=path or basename != name:

            # clear current selection (otherwise multiple items will be selected)
            self.selectionModel().setCurrentIndex( self.currentIndex(), QtGui.QItemSelectionModel.Clear )
            
            # set new current item
            index = self.model().index( path + "/" + name)
            self.selectionModel().setCurrentIndex(index,QtGui.QItemSelectionModel.Select )




    def currentImage(self):
        """
        returns current image selected
        """
        return self.model().imageInfo( self.currentIndex() )

    def keyPressEvent(self, event):

        # print "[ImageDirectoryManager] key pressed",event.key()
        if type(event) == QtGui.QKeyEvent:

            if event.key()==QtCore.Qt.Key_Return:
                # if return key is pressed, expand/collapse current directory 
                index = self.currentIndex()
                if self.model().isDir(index):
                    self.setExpanded(index, not self.isExpanded(index) )
            elif event.key()==QtCore.Qt.Key_Space:
                # if space key is pressed, emit image selected signal
                index = self.currentIndex()
                if not self.model().isDir(index):
                    self.model().toggleCheckState( index )
            else:
                QtGui.QTreeView.keyPressEvent(self, event)

class ImageViewer(QtGui.QLabel):

    maxSize = 800

    def __init__(self):
        QtGui.QLabel.__init__(self)
        # self.setStyleSheet("border: 10px solid  #ff0000 ;")
        self.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        # self.setFixedWidth(self.maxSize + 40 )

    def setBorderColor(self,color):
        self.setStyleSheet("border: 10px solid  "+color+" ;")

        # if state:
            # print "[ImageViewer] set border color True"
            # self.setStyleSheet("border: 10px solid  "+color+"#ff0000 ;")
            # self.setStyleSheet("border-color: #ff0000 ;")
        # else:
            # print "[ImageViewer] set border color False"
            # self.setStyleSheet("border: 10px solid  #00ff00 ;")
            # self.setStyleSheet("border-color: #00ff00 ;")
        # self.update()

    def currentImage(self):
        return self.current_path,self.current_name

    def showImage(self, path,name):
        self.pixmap = QtGui.QPixmap(path+'/'+name)
        self.current_path = path
        self.current_name = name
        # print "[ImageViewer::showImage]",filename,"%d/%d"%(pixmap.width(),pixmap.height())

        maxSize = 800

        if self.pixmap.width()>maxSize and self.pixmap.width()>self.pixmap.height():
            self.setPixmap(self.pixmap.scaledToWidth(maxSize))
        elif self.pixmap.height()>maxSize and self.pixmap.height()>self.pixmap.width():
            self.setPixmap(self.pixmap.scaledToHeight(maxSize))
        else:
            self.setPixmap(self.pixmap)
        
        # if self.pixmap.width() > 1024:
        #     self.setPixmap(self.pixmap.scaledToWidth(1024))
        # else:
        #     self.setPixmap(self.pixmap)

class MainWindow(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setWindowTitle("OurAwesomeImageViewer... :)")
        self.initImageViewer()
        self.initImageDirectoryManager()
        self.initSelectionManager()
        self.initToolBar()

        self.image_directory_manager.currentImageChanged.connect( lambda path,name : self.viewer.showImage(path,name) )        
        self.image_directory_manager.currentImageChanged.connect( lambda path,name : self.selection_view.setCurrentImage(path,name) )        
        self.image_directory_manager.currentImageChanged.connect( lambda path,name : self.viewer.setBorderColor(["#ff0000","#00ff00"][self.selection_model.hasImage(path,name)])  )

        self.selection_model.imageAdded.connect( lambda: self.viewer.setBorderColor("#00ff00") )
        self.selection_model.imageRemoved.connect( lambda: self.viewer.setBorderColor("#ff0000") )

        self.selection_view.currentImageChanged.connect( self.image_directory_manager.setCurrentImage )

        self.selection_model.load(QtCore.QString("/home/mrdut/Pictures2/Vacances/Canada/canadanna.sel"))

    def initToolBar(self):
        toolbar = self.addToolBar("File")

        self.exit_action = QtGui.QAction(QtGui.QIcon.fromTheme("application-exit"),"&exit",self)
        self.exit_action.setShortcut(QtGui.QKeySequence("Alt+F4"))
        self.exit_action.triggered.connect(self.close)

        self.open_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-open"),"&open",self)
        self.open_action.setShortcut(QtGui.QKeySequence("Ctrl+O"))
        self.open_action.triggered.connect(self.selection_view.load)

        self.save_as_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-save-as"),"&save as",self)
        self.save_as_action.setShortcut(QtGui.QKeySequence("Ctrl+S"))
        self.save_as_action.triggered.connect(self.selection_view.saveAs)

        self.export_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-send"),"&export",self)
        self.export_action.setShortcut(QtGui.QKeySequence("Ctrl+E"))
        self.export_action.triggered.connect(self.selection_view.export)

        toolbar.addAction(self.exit_action)
        toolbar.addSeparator()
        toolbar.addAction(self.open_action)
        toolbar.addAction(self.save_as_action)
        toolbar.addSeparator()
        toolbar.addAction(self.export_action)

    def initImageViewer(self):
        self.viewer = ImageViewer()
        self.setCentralWidget(self.viewer)

    def initImageDirectoryManager(self):
        self.image_directory_manager = ImageDirectoryManager()

        dock = QtGui.QDockWidget()
        dock.setWidget(self.image_directory_manager)
        dock.setFeatures(QtGui.QDockWidget.NoDockWidgetFeatures)
        dock.setTitleBarWidget(QtGui.QWidget())
        dock.setMinimumWidth(500)

        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea,dock)

    def initSelectionManager(self):
        # self.selection_model = SelectionManager()
        self.selection_model = self.image_directory_manager.model().selection_model
        self.selection_view = SelectionView(self.selection_model)


        dock = QtGui.QDockWidget()
        dock.setWidget(self.selection_view)
        dock.setFeatures(QtGui.QDockWidget.NoDockWidgetFeatures)
        dock.setTitleBarWidget(QtGui.QWidget())
        dock.setMinimumWidth(350)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea,dock)

    def keyPressEvent(self, event):

        # print "[MainWindow] key pressed",event
        if type(event) == QtGui.QKeyEvent:
            key = event.key()
            # print "[key pressed]",event.key(),QtCore.Qt.Key_Return
            if key==QtCore.Qt.Key_Q or key==QtCore.Qt.Key_Escape : # (Q)uit
                self.close()
            # elif key==QtCore.Qt.Key_Return:
            #     self.switchCurrentImage()
            # else:
                # print "key pressed"

    def closeEvent(self,event):

        if self.selection_view.changed():
            self.selection_view.saveAs()

        event.accept()

if __name__ == "__main__":

    app = QtGui.QApplication(sys.argv)
    win = MainWindow()
    win.showMaximized()
    # win.showFullScreen()
    app.exec_()
