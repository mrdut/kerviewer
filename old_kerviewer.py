#!/usr/bin/python

import os, sys
import Image
from PyQt4 import QtGui,QtCore




def createThumbnailDirectory(path='./data'):

    if not os.path.exists(path + '/thumbnail/'):
        print 'making directory',path + '/thumbnail/'
        os.makedirs(path + '/thumbnail/')

    for f in os.listdir( path ):
        if f.endswith('.JPG'):
            if not os.path.isfile(path + '/thumbnail/' + f ):
                print 'creating',path + '/thumbnail/' + f
                im = Image.open(path + '/' + f)
                im.thumbnail((128, 128), Image.ANTIALIAS)
                im.save(path + '/thumbnail/' + f, "JPEG")

#~ class ImageTreeItem(QtGui.QTreeWidgetItem):
    #~ def __init__(self,image,text):
        #~ QtGui.QTreeWidgetItem.__init__(self)
        #~ self._image = image
        #~ self.setText(0, text)
        #~ self.setIcon(0,QtGui.QIcon.fromTheme("image-x-generic"))
#~ 
    #~ def setFocusOnImage(self):
        #~ self.image.setFocus(QtCore.Qt.OtherFocusReason)
#~ 
    #~ def image(self):
        #~ return self._image
        
class ImageButton(QtGui.QAbstractButton):

    fixed_size = QtCore.QSize(128+30,128+50)

    def __init__(self,fullpath,parent=None):
        QtGui.QAbstractButton.__init__(self,parent)

        self.setCheckable(True)
        self.fullpath = fullpath
        self.setText(os.path.basename(str(fullpath)))
        self.pixmap = QtGui.QPixmap(fullpath)
        #~ self.setToolTip(fullpath)

        self.rotated = 0

        # assert all thumbnail fits in 128x128
        #~ self.setFixedSize(self.sizeHint())
        self.setFixedSize(ImageButton.fixed_size)
        #~ print self.sizeHint(),self.size()
        #~ self.tree_item = None
    
    def dirname(self):
        return os.path.dirname(self.fullpath)
    
    def basename(self):
        return os.path.basename(self.fullpath)
    
    #~ def item(self):
        #~ self.tree_item is None and self.initTreeItem()
        #~ return self.tree_item

    def initTreeItem(self):
        self.tree_item = ImageTreeItem(self,self.text())

    def closeEvent(self,e):
        print "close"
        if self.rotated:
            print self.text(),"was rotated",self.rotated%360
            
            #~ im1 = Image.open("Donald.gif")
            #~ im2 = im1.rotate(60)
            #~ im2.save("d.gif")
            
        QtGui.QAbstractButton.closeEvent(self,e)

    def rotate(self,angle):
        transform = QtGui.QTransform()  
        transform.rotate(angle)
        self.rotated+=angle
        self.setPixmap( self.pixmap.transformed(transform) ) 

    def setPixmap(self, pm ):
        self.pixmap = pm
        self.update()

    def sizeHint(self):
        return ImageButton.fixed_size
        #~ return self.pixmap.rect().size() + QtCore.QSize(30,50)
        #~ return self.pixmap.rect().size() + QtCore.QSize(30,50)
        #~ return QtCore.QSize(128+30,128+50)

    # there is one paintEvent for mousePressEvent and one for mouseReleaseEvent
    # we only want one repaint, otherwise there is a display bug (troubleshouting because of different color according to focus)
    # just ignore mousePressEvent, but we have to set checked state and emit clicked signal (checked state is change when there is a combination of mouse press and release event)
    def mousePressEvent (self,  e ):
        e.ignore()
        self.setChecked(not self.isChecked())
        self.clicked.emit(self.isChecked())
        #~ print "\nmousePressEvent",self.text()
        #~ return QtGui.QAbstractButton.mousePressEvent(self,e)
        
    #~ def mouseReleaseEvent (self, e ):
        #~ print "mouseReleaseEvent",self.text()
        #~ return QtGui.QAbstractButton.mouseReleaseEvent(self,e)

    def paintEvent(self, event ):
        painter = QtGui.QPainter( self )
        #~ print "paintEvent",self.text()
        #~ ,self.receivers()

        # draw image
        rect = self.pixmap.rect()
        width = rect.width()
        height = rect.height()
        offset_height = (128-height)*0.5
        offset_width = (128-width)*0.5
        painter.drawPixmap( 15 + offset_width, 15 + offset_height, self.pixmap )

        # draw text within a 128x20 box
        painter.drawText( QtCore.QRect( 15 , 20+height+offset_height , 128,20 ), QtCore.Qt.AlignCenter ,self.text() )

        # draw a 7 pixels (=>set pen to 15 since half of pen width is outside de plot) large border if image is selected
        if self.isChecked():
            color = [QtGui.QColor('darkBlue'),QtGui.QColor('darkRed')][self.hasFocus()]
            painter.setPen(QtGui.QPen( color , 15))
            size = self.size()
            painter.drawRect( 0,0,size.width(),size.height() )

class ImageFolder(QtGui.QScrollArea):
    def __init__(self,fullpath,parent=None):
        QtGui.QScrollArea.__init__(self,parent)

        self.fullpath = fullpath

        widget = QtGui.QWidget(self)
        grid = QtGui.QGridLayout(self)
        widget.setLayout(grid)

        # add images from directory
        print "only works with .JPG"
        images = [ f for f in os.listdir(fullpath) if f.endswith('.JPG')]

        progress_bar = QtGui.QProgressBar()
        progress_bar.setMaximum(len(images))
        progress_bar.show()

        QtGui.QApplication.processEvents()

        thumbnail_path = str(fullpath) + '/thumbnail/'

        if not os.path.exists(thumbnail_path):
            print 'making directory',thumbnail_path
            os.makedirs(thumbnail_path)
        
        for i,img in enumerate(images):
            col = i%9
            row = (i-col)/9

            path = thumbnail_path + img
            if not os.path.isfile(path):

                print 'creating',path
                im = Image.open(str(fullpath) + '/' + img)
                im.thumbnail((128, 128), Image.ANTIALIAS)
                im.save(path, "JPEG")

            button = ImageButton(path , self)
            grid.addWidget(button,row,col)
            QtCore.QObject.connect(button, QtCore.SIGNAL("clicked()"),self.onImageClicked )

            progress_bar.setValue(i)

        self.setWidget(widget)

    def onImageClicked(self):
        image = self.sender()
        print "[ImageFolder]",image,'was clicked'
        self.emit(QtCore.SIGNAL("imageClicked(PyQt_PyObject)"),image)

    def setColumnCount(self,ncolumn):
        widget = self.widget()
        grid = widget.layout()

        widget.hide()
        nitems = grid.count()
        layout_items = [grid.takeAt(i) for i in range(nitems-1,-1,-1) ]
 
        QtGui.QWidget().setLayout(grid) # set current layout to dumb widget -> easiest way to remove layout from widget
        grid = QtGui.QGridLayout()      # create new layout
        widget.setLayout(grid)          # set new layout to widget
        
        for i,item in enumerate(layout_items):
            col = i%ncolumn
            row = (i-col)/ncolumn
            grid.addWidget(item.widget(),row,col)
        
        width = (ImageButton.fixed_size.width()+grid.verticalSpacing()) * grid.columnCount()
        height = (ImageButton.fixed_size.height()+grid.horizontalSpacing()) * grid.rowCount()
        widget.resize(width,height)
        widget.show()
    
class ImageFolderManager(QtGui.QTabWidget):
    def __init__(self,parent=None):
        QtGui.QTabWidget.__init__(self,parent)

        self.addTab(QtGui.QWidget(),QtGui.QIcon.fromTheme("list-add"),QtCore.QString())

        self.previous_index = -1
        self.currentChanged.connect(self.setCurrentFolder)
        
        self.current_image = None
        
    def setCurrentFolder(self,index):
        if index == self.count()-1:
            self.setCurrentIndex(self.previous_index)
            fullpath = QtGui.QFileDialog.getExistingDirectory(self, "Open Image Directory", "heyheyhey", QtGui.QFileDialog.ShowDirsOnly)
            if fullpath.isEmpty(): return
            for i in range(self.count()-1):
                if self.widget(i).fullpath == fullpath:
                    print "same folder already open - skip -",fullpath
                    return
            self.openDirectory(fullpath)

        else:
            self.previous_index=index

    def currentImage(self):
        return self.current_image

    def currentFolder(self):
        return self.currentWidget()

    def setCurrentImage(self,image):    
        print "[ImageFolderManager] current image is now",image
        self.current_image = image
        self.emit(QtCore.SIGNAL("currentImageChanged(PyQt_PyObject)"),image)

    def openDirectory(self, path):
        folder = ImageFolder(path,self)
        self.connect(folder,QtCore.SIGNAL("imageClicked(PyQt_PyObject)"),self.setCurrentImage)
        self.insertTab(self.count()-1,folder,QtGui.QIcon.fromTheme("folder"), os.path.basename(str(path)))

        self.setCurrentIndex(self.count()-2)
        self.emit(QtCore.SIGNAL("folderOpened(QString)"),path)


class Selection(QtGui.QTreeWidget):
    default_fname = "untitled"

    def __init__(self,parent=None):
        QtGui.QTreeWidget.__init__(self,parent)
        self.fname = Selection.default_fname
        self.header().hide()
        self.cached_data = []

    def addItem(self,image):
        print "[Selection] add/remove ",image

        dirname = image.dirname()
        basename = image.basename()

        branch = self.findItems(dirname,QtCore.Qt.MatchExactly)
        if branch==[]:
            branch = QtGui.QTreeWidgetItem(self)
            branch.setText(0, dirname)
            branch.setIcon(0,QtGui.QIcon.fromTheme("folder"))
        else:
            branch=branch[0]

        if image.isChecked():
            item = QtGui.QTreeWidgetItem(branch)
            item.setText(0, basename)
            item.setIcon(0,QtGui.QIcon.fromTheme("image-x-generic"))
        else:
            for i in range(branch.childCount()):
                print i,branch.child(i)
                if branch.child(i).text(0)==basename:
                    branch.takeChild(i)
        
        if branch.childCount()==0:
            self.takeTopLevelItem( self.indexOfTopLevelItem(branch) )

    def name(self):
        return os.path.splitext(self.basename())[0]

    def isEmpty(self):
        return self.topLevelItemCount()==0

    def basename(self):
        return os.path.basename(self.fname)

    def load(self,fname):
        self.fname = fname
        f = open(str(fname),'r')
        self.cache = f.readlines()
        f.close()
        self.emit()
        
    def save(self,fname):
        print "[Selection] save to",fname
        
        self.fname = fname

        root = self
        selection = []
        for i in range(root.topLevelItemCount()):
            directory = root.topLevelItem(i)
            path = str(directory.text(0))
            
            for j in range(directory.childCount()):
                img = directory.child(j)
                print "\t",img.text(0)
                selection.append( path + '/' + str(img.text(0)) )

        f = open(str(fname),'w')
        f.write( '\n'.join(selection) )
        f.close()

class SelectionManager(QtGui.QWidget):
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        box = QtGui.QVBoxLayout(self)
        
        self.combo = QtGui.QComboBox()
        
        self.stack = QtGui.QStackedLayout()

        self.toolbar = self.createToolBar()

        self.combo.currentIndexChanged.connect(self.setCurrentSelection)

        self.current_selection = None

        box.addWidget(self.combo)
        box.addLayout(self.stack)

    def currentSelection(self):
        return self.current_selection

    def addItemToCurrentSelection(self,image):
        print "[SelectionManager] add item to current selection",image
        self.currentSelection().addItem(image)

    def createToolBar(self):
        save_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-save"),"save current selection",self)
        save_action.triggered.connect(self.saveCurrentSelection)

        save_as_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-save-as"),"save as current selection",self)
        save_as_action.triggered.connect(self.saveAsCurrentSelection)


        new_selection_action = QtGui.QAction(QtGui.QIcon.fromTheme("list-add"),"new selection",self)
        new_selection_action.setShortcut(QtGui.QKeySequence("Ctrl+n"))
        new_selection_action.triggered.connect(self.createNewSelection)

        remove_selection_action = QtGui.QAction(QtGui.QIcon.fromTheme("list-remove"),"close selection",self)

        toolbar = QtGui.QToolBar(None)
        toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonFollowStyle)        

        toolbar.addAction(save_action)
        toolbar.addAction(save_as_action)
        
        toolbar.addAction(new_selection_action)
        toolbar.addAction(remove_selection_action)
        
        return toolbar

    def setCurrentSelection(self,index):
        print "[SelectionManager] set current selection",index
        self.current_selection = self.stack.widget(index)
        self.stack.setCurrentWidget(self.current_selection)
        self.emit(QtCore.SIGNAL("currentSelectionChanged()"))

    def removeSelection(self):
        index = self.combo.currentIndex()
        self.stack.removeWidget(self.stack.widget(index))
        self.combo.removeItem(index)
        
    def createNewSelection(self):
        if self.currentSelection() and self.currentSelection().isEmpty() and self.currentSelection().name()=="untitled": return
        
        print "[SelectionManager] create new selection"
        selection = Selection()
        self.stack.addWidget(selection)
        self.combo.addItem(selection.name())
        
    def deleteCurrentSelection(self):
        pass

    def loadSelection(self,fname):
        pass
        
    def saveCurrentSelection(self,fname):
        
        if fname.isEmpty() or fname==Selection.default_fname:
            return self.saveAsCurrentSelection()
        
        print "saving current selection to",fname
        
        self.current_selection.save(fname)

    def saveAsCurrentSelection(self):
        fname = QtGui.QFileDialog.getSaveFileName(self,"Save Selection As","./untitled.sel","Selctions (*.sel)")

        if not fname.isEmpty():
            self.saveCurrentSelection(fname)

    def deleteSelection(self,fname):
        pass
   
class PhotoViewer(QtGui.QMainWindow):
    def __init__(self,parent=None):
        QtGui.QMainWindow.__init__(self,parent)

        self.resize(1800, 1000)
        self.setWindowTitle("Photo Viewer")



        self.current_image = None

        folder_manager = ImageFolderManager(self)
        
        self.setCentralWidget(folder_manager)
        self.connect(self.centralWidget(),QtCore.SIGNAL("folderOpened(QString)"),self.onOpenedFolder)
        #~ self.connect(self.centralWidget(),QtCore.SIGNAL("currentImageChanged()"),self.onImageSelected)


        self.createDockWindow()
        self.createToolBar()


        self.connect(folder_manager,QtCore.SIGNAL("currentImageChanged(PyQt_PyObject)"),self.selection_manager.addItemToCurrentSelection)

    def onOpenedFolder(self,path):
        #~ selection = self.selection_manager.currentSelection()
        #~ if self.selection_tree.findItems(path,QtCore.Qt.MatchExactly)==[]:
            #~ item = QtGui.QTreeWidgetItem(self.selection_tree)
            #~ item.setText(0, path)
            #~ item.setIcon(0,QtGui.QIcon.fromTheme("folder"))
#~ 
        pass


    def createDockWindow(self):
        self.selection_manager = SelectionManager()
        self.selection_manager.createNewSelection()

        dock = QtGui.QDockWidget(self,)
        dock.setFeatures(QtGui.QDockWidget.NoDockWidgetFeatures)
        dock.setWidget(self.selection_manager)
        dock.setTitleBarWidget(QtGui.QWidget())        
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea,dock)
        
        #~ self.selection_tree.itemPressed.connect(self.onTreeItemPressed)

    def onTreeItemPressed(self,item,column):
        if type(item) is ImageTreeItem:
            item.setFocusOnImage()
            self.centralWidget().currentWidget().ensureWidgetVisible(item.image)

    def setColumnCount(self,ncolumn):
        self.centralWidget().currentWidget().setColumnCount(ncolumn)

    def rotateRightCurrentImage(self):
        if self.current_image is None : return
        self.current_image.rotate(90)

    def rotateLeftCurrentImage(self):
        if self.current_image is None : return
        #~ print "rotate left",self.current_image.text()
        self.current_image.rotate(-90)

    def createToolBar(self):

        
        self.application_exit_action = QtGui.QAction(QtGui.QIcon.fromTheme("application-exit"),"&exit",self)
        self.application_exit_action.setShortcut(QtGui.QKeySequence("Alt+F4"))
        self.application_exit_action.triggered.connect(self.close)

        self.rotate_left_action = QtGui.QAction(QtGui.QIcon.fromTheme("object-rotate-left"),"&rotateLeft",self)
        self.rotate_left_action.triggered.connect(self.rotateLeftCurrentImage)

        rotate_right_action = QtGui.QAction(QtGui.QIcon.fromTheme("object-rotate-right"),"&rotateRight",self)
        rotate_right_action.triggered.connect(self.rotateRightCurrentImage)

        spinbox = QtGui.QSpinBox(self)
        spinbox.setRange(1,50)
        spinbox.setValue(9)
        self.connect(spinbox,QtCore.SIGNAL("valueChanged(int)"),self.setColumnCount)


        toolbar = self.addToolBar("File")
        toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonFollowStyle)        

        toolbar.addAction(self.application_exit_action)

        toolbar.addSeparator()
    
        toolbar.addAction(self.rotate_left_action)
        toolbar.addAction(rotate_right_action)

        toolbar.addSeparator()
        toolbar.addWidget(spinbox)        

        self.addToolBar(self.selection_manager.toolbar)


        #~ for icon in ["document-new","document-open","document-open-recent",,"document-save-as","edit-clear","edit-delete","edit-select-all","edit-find"]:
            #~ action = QtGui.QAction(QtGui.QIcon.fromTheme(icon),icon,self)
            #~ toolbar.addAction(action)



    #~ def onImageClicked(self,image,state):
        #~ folder = self.sender()
        #~ print "[PhotoViewer]",folder,image,'was clicked - state is',state

    #~ def onImageSelected(self):
#~ 
        #~ self.current_image = self.centralWidget().currentImage()
#~ 
        #~ path = self.centralWidget().currentWidget().fullpath
        #~ root = self.selection_tree.findItems(path,QtCore.Qt.MatchExactly)[0]
        #~ if self.current_image.isChecked():
            #~ root.addChild( self.current_image.item() )
        #~ else:
            #~ root.takeChild( root.indexOfChild ( self.current_image.item() ) )

           
if __name__ == "__main__":

    app = QtGui.QApplication(sys.argv)

    photo_viewer = PhotoViewer()
    photo_viewer.show()
    # photo_viewer.centralWidget().openDirectory('/home/mrdut/workspace/python/kerviewer/data')

    app.exec_()







#~ for i in range(3):
    #~ for j in range(4):
        #~ grid.addWidget(ImageButton('/home/florent/workspace/python/photo_viewer/data/thumbnail/DSCN2439.JPG',main_widget),i,j)

#~ 
#~ 
#~ import os
#~ import time
#~ import glob
#~ 
#~ from PyQt4 import QtCore, QtGui
#~ 
#~ class Ui_MainWindow(object):
    #~ def setupUi(self, MainWindow):
        #~ MainWindow.resize(900, 600)
        #~ self.centralwidget = QtGui.QWidget(MainWindow)
        #~ self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        #~ self.verticalLayout = QtGui.QVBoxLayout()
        #~ self.scene = QtGui.QGraphicsScene()
        #~ self.view = QtGui.QGraphicsView(self.scene)
        #~ self.verticalLayout.addWidget(self.view)
        #~ self.horizontalLayout = QtGui.QHBoxLayout()
        #~ spacerItem = QtGui.QSpacerItem(150, 20, QtGui.QSizePolicy.Expanding,
                                        #~ QtGui.QSizePolicy.Minimum)
        #~ self.horizontalLayout.addItem(spacerItem)
        #~ self.toolButton_3 = QtGui.QToolButton(self.centralwidget)
        #~ self.toolButton_3.setIconSize(QtCore.QSize(48, 24))
        #~ self.toolButton_3.setText("Previous")
        #~ self.horizontalLayout.addWidget(self.toolButton_3)
        #~ self.toolButton_4 = QtGui.QToolButton(self.centralwidget)
        #~ self.toolButton_4.setIconSize(QtCore.QSize(48, 24))
        #~ self.toolButton_4.setText("Next")
        #~ self.horizontalLayout.addWidget(self.toolButton_4)
        #~ spacerItem1 = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Expanding,
                                        #~ QtGui.QSizePolicy.Minimum)
        #~ self.horizontalLayout.addItem(spacerItem1)
        #~ self.toolButton_6 = QtGui.QToolButton(self.centralwidget)
        #~ self.toolButton_6.setText("Quit")
        #~ self.horizontalLayout.addWidget(self.toolButton_6)
        #~ self.verticalLayout.addLayout(self.horizontalLayout)
        #~ self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        #~ MainWindow.setCentralWidget(self.centralwidget)
        #~ MainWindow.setWindowTitle("speedyView")
        #~ MainWindow.show()
        #~ QtCore.QCoreApplication.processEvents()
#~ 
        #~ QtCore.QObject.connect(ui.toolButton_3, QtCore.SIGNAL("clicked()"), self.prec)
        #~ QtCore.QObject.connect(ui.toolButton_4, QtCore.SIGNAL("clicked()"), self.next)
        #~ QtCore.QObject.connect(ui.toolButton_6, QtCore.SIGNAL("clicked()"), exit)
        #~ QtCore.QMetaObject.connectSlotsByName(MainWindow)
#~ 
        #~ self.centralwidget.wheelEvent = self.wheel_event
#~ 
        #~ self.set_view()
#~ 
    #~ def set_view(self):
        #~ in_folder = "/folder/with/images/"
        #~ chain = in_folder + "/*"
        #~ self.images = glob.glob(chain)
        #~ self.images.sort(cmp=lambda x, y: cmp(x.lower(), y.lower()))
        #~ self.zoom_step = 0.04
        #~ self.w_vsize = self.view.size().width()
        #~ self.h_vsize = self.view.size().height()
        #~ if self.w_vsize <= self.h_vsize:
            #~ self.max_vsize = self.w_vsize
        #~ else:
            #~ self.max_vsize = self.h_vsize
        #~ self.l_pix = ["", "", ""]
#~ 
        #~ self.i_pointer = 0
        #~ self.p_pointer = 0
        #~ self.load_current()
        #~ self.p_pointer = 1
        #~ self.load_next()
        #~ self.p_pointer = 2
        #~ self.load_prec()
        #~ self.p_pointer = 0
#~ 
    #~ def next(self):
        #~ self.i_pointer += 1
        #~ if self.i_pointer == len(self.images):
            #~ self.i_pointer = 0
        #~ self.p_view = self.c_view
        #~ self.c_view = self.n_view
        #~ self.view_current()
        #~ if self.p_pointer == 0:
            #~ self.p_pointer = 2
            #~ self.load_next()
            #~ self.p_pointer = 1
        #~ elif self.p_pointer == 1:
            #~ self.p_pointer = 0
            #~ self.load_next()
            #~ self.p_pointer = 2
        #~ else:
            #~ self.p_pointer = 1
            #~ self.load_next()
            #~ self.p_pointer = 0
#~ 
    #~ def prec(self):
        #~ self.i_pointer -= 1
        #~ if self.i_pointer <= 0:
            #~ self.i_pointer = len(self.images)-1
        #~ self.n_view = self.c_view
        #~ self.c_view = self.p_view
        #~ self.view_current()
        #~ if self.p_pointer == 0:
            #~ self.p_pointer = 1
            #~ self.load_prec()
            #~ self.p_pointer = 2
        #~ elif self.p_pointer == 1:
            #~ self.p_pointer = 2
            #~ self.load_prec()
            #~ self.p_pointer = 0
        #~ else:
            #~ self.p_pointer = 0
            #~ self.load_prec()
            #~ self.p_pointer = 1
#~ 
       #~ 
    #~ def view_current(self):
        #~ size_img = self.c_view.size()
        #~ wth, hgt = QtCore.QSize.width(size_img), QtCore.QSize.height(size_img)
        #~ self.scene.clear()
        #~ self.scene.setSceneRect(0, 0, wth, hgt)
        #~ self.scene.addPixmap(self.c_view)
        #~ QtCore.QCoreApplication.processEvents()
#~ 
    #~ def load_current(self):
        #~ self.l_pix[self.p_pointer] = QtGui.QPixmap(self.images[self.i_pointer])
        #~ self.c_view = self.l_pix[self.p_pointer].scaled(self.max_vsize, self.max_vsize, 
                                            #~ QtCore.Qt.KeepAspectRatio, 
                                            #~ QtCore.Qt.FastTransformation)
        #~ #change the previous line with QtCore.Qt.SmoothTransformation eventually
        #~ self.view_current()
#~ 
    #~ def load_next(self):
        #~ if self.i_pointer == len(self.images)-1:
            #~ p = 0
        #~ else:
            #~ p = self.i_pointer + 1
        #~ self.l_pix[self.p_pointer] = QtGui.QPixmap(self.images[p])
        #~ self.n_view = self.l_pix[self.p_pointer].scaled(self.max_vsize, 
                                            #~ self.max_vsize, 
                                            #~ QtCore.Qt.KeepAspectRatio, 
                                            #~ QtCore.Qt.FastTransformation)
#~ 
    #~ def load_prec(self):
        #~ if self.i_pointer == 0:
            #~ p = len(self.images)-1
        #~ else:
            #~ p = self.i_pointer - 1
        #~ self.l_pix[self.p_pointer] = QtGui.QPixmap(self.images[p])
        #~ self.p_view = self.l_pix[self.p_pointer].scaled(self.max_vsize, 
                                            #~ self.max_vsize, 
                                            #~ QtCore.Qt.KeepAspectRatio, 
                                            #~ QtCore.Qt.FastTransformation)
#~ 
    #~ def wheel_event (self, event):
        #~ numDegrees = event.delta() / 8
        #~ numSteps = numDegrees / 15.0
        #~ self.zoom(numSteps)
        #~ event.accept()
#~ 
    #~ def zoom(self, step):
        #~ self.scene.clear()
        #~ w = self.c_view.size().width()
        #~ h = self.c_view.size().height()
        #~ w, h = w * (1 + self.zoom_step*step), h * (1 + self.zoom_step*step)
        #~ self.c_view = self.l_pix[self.p_pointer].scaled(w, h, 
                                            #~ QtCore.Qt.KeepAspectRatio, 
                                            #~ QtCore.Qt.FastTransformation)
        #~ self.view_current()
#~ 
#~ if __name__ == "__main__":
    #~ import sys
    #~ app = QtGui.QApplication(sys.argv)
    #~ MainWindow = QtGui.QMainWindow()
    #~ ui = Ui_MainWindow()
    #~ ui.setupUi(MainWindow)
    #~ MainWindow.show()
    #~ sys.exit(app.exec_())
